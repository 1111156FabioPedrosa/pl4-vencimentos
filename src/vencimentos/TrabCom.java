/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vencimentos;

public class TrabCom extends Trabalhador {

    private float salario;
    private float vendas;
    private float comissao;

    public TrabCom(String nome, float salario, float vendas, float comissao) {
        super(nome);
        setSalario(salario);
        setVendas(vendas);
        setComissao(comissao);
    }

    public TrabCom(String nome) {
        this(nome, 0, 0, 0);
    }

    public TrabCom() {
        this("", 0, 0, 0);
    }

    public TrabCom(TrabCom t) {
        super(t);
        salario = t.salario;
        vendas = t.vendas;
        comissao = t.comissao;
    }

    public float getSalario() {
        return salario;
    }

    public float getVendas() {
        return vendas;
    }

    public float getComissao() {
        return comissao;
    }

    public void setSalario(float salario) {
        this.salario = salario >= 485 ? salario : 485;
    }

    public void setVendas(float vendas) {
        this.vendas = vendas >= 0 ? vendas : 0;
    }

    public void setComissao(float comissao) {
        this.comissao = (comissao >= 0 && comissao <= 100) ? comissao : 0;
    }

    public String toString() {
        return String.format("Trabalhador à Comissão: %s %nSalário: %.2f € %nVendas: %.2f € %nComissão: %.1f%%",
                super.toString(), salario, vendas, comissao);
    }

    public float vencimento() {
        return salario + vendas * (comissao / 100);
    }
}
