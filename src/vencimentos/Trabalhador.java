package vencimentos;


public abstract class Trabalhador {

    private String nome;

    public Trabalhador(String nome) {
        setNome(nome);
    }

    public Trabalhador() {
        setNome("");
    }

    public Trabalhador(Trabalhador t) {
        nome = t.nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = (nome == null || nome.isEmpty()) ? "sem nome" : nome;
    }

    public String toString() {
        return nome;
    }

    public abstract float vencimento();
}
