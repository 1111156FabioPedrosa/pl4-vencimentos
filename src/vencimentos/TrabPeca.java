package vencimentos;


public class TrabPeca extends Trabalhador {

    private static float pagPeca1 = 15.5f;
    private static float pagPeca2 = 20;
    private int numPecas1;
    private int numPecas2;

    public TrabPeca(String nome, int numPecas1, int numPecas2) {
        super(nome);
        setNumPecas1(numPecas1);
        setNumPecas2(numPecas2);
    }

    public TrabPeca(String nome) {
        this(nome, 0, 0);
    }

    public TrabPeca() {
        this("", 0, 0);
    }

    public TrabPeca(TrabPeca t) {
        super(t);
        numPecas1 = t.numPecas1;
        numPecas2 = t.numPecas2;
    }

    public static float getPagPeca1() {
        return pagPeca1;
    }

    public static float getPagPeca2() {
        return pagPeca2;
    }

    public static void setPagPeca1(float pagPeca1) {
        if (pagPeca1 > 0) {
            TrabPeca.pagPeca1 = pagPeca1;
        }
    }

    public static void setPagPeca2(float pagPeca2) {
        if (pagPeca2 > 0) {
            TrabPeca.pagPeca2 = pagPeca2;
        }
    }

    public int getNumPecas1() {
        return numPecas1;
    }

    public int getNumPecas2() {
        return numPecas2;
    }

    public void setNumPecas1(int numPecas1) {
        this.numPecas1 = numPecas1 >= 0 ? numPecas1 : 0;
    }

    public void setNumPecas2(int numPecas2) {
        this.numPecas2 = numPecas2 >= 0 ? numPecas2 : 0;
    }

    public String toString() {
        return String.format("Trabalhador à Peça: %s %nNº de Peças 1: %d %nNº de Peças 2: %d",
                super.toString(), numPecas1, numPecas2);
    }

    public float vencimento() {
        return numPecas1 * pagPeca1 + numPecas2 * pagPeca2;
    }
}