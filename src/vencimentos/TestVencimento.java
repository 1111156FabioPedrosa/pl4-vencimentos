/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vencimentos;

/**
 *
 * @author fabio
 */
public class TestVencimento {

    public static void main(String[] args) {
        TrabPeca tp = new TrabPeca("Jorge Silva", 20, 30);
        TrabCom tc = new TrabCom("Susana Ferreira", 500, 1500, 6);
        TrabHora th = new TrabHora("Carlos Miguel", 160, (float) 3.5);
        Trabalhador[] trabalhadores = new Trabalhador[3];
        trabalhadores[0] = tp;
        trabalhadores[1] = tc;
        trabalhadores[2] = th;
        
        for (int i = 0; i < trabalhadores.length; i++) {
            System.out.println(trabalhadores[i].vencimento());
        }
        
        System.out.println("");
        for (int i = 0; i < trabalhadores.length; i++) {
            if (trabalhadores[i] instanceof TrabHora) {
                System.out.println(trabalhadores[i]);
            }
        }
    }
}
