/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vencimentos;

public class TrabHora extends Trabalhador {

    private int numHoras;
    private float pagHora;

    public TrabHora(String nome, int numHoras, float pagHora) {
        super(nome);
        setNumHoras(numHoras);
        setPagHora(pagHora);
    }

    public TrabHora(String nome) {
        this(nome, 0, 0);
    }

    public TrabHora() {
        this("", 0, 0);
    }

    public TrabHora(TrabHora t) {
        super(t);
        numHoras = t.numHoras;
        pagHora = t.pagHora;
    }

    public int getNumHoras() {
        return numHoras;
    }

    public float getPagHora() {
        return pagHora;
    }

    public void setNumHoras(int numHoras) {
        this.numHoras = numHoras >= 0 ? numHoras : 0;
    }

    public void setPagHora(float pagHora) {
        this.pagHora = pagHora > 0 ? pagHora : 10;
    }

    public String toString() {
        return String.format("Trabalhador à Hora: %s %nNº de Horas: %d %nPagamento Hora: %.2f €",
                super.toString(), numHoras, pagHora);
    }

    public float vencimento() {
        return numHoras * pagHora;
    }
}